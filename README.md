# Pintu-monorepo-be-golang

## Overview

This repository contains the Kubernetes manifests for deploying the `pintu-monorepo-be-golang` application. It uses [Kustomize](https://kustomize.io/) to manage the configuration and [ArgoCD](https://argo-cd.readthedocs.io/) for continuous deployment.

## Table of Contents

- [Pintu-monorepo-be-golang](#pintu-monorepo-be-golang)
  - [Overview](#overview)
  - [Table of Contents](#table-of-contents)
  - [Prerequisites](#prerequisites)
  - [Directory Structure](#directory-structure)

## Prerequisites

- [Kustomize](https://kustomize.io/)
- [ArgoCD](https://argo-cd.readthedocs.io/)

## Directory Structure

```
.
├── .editorconfig
├── README.md
├── base
│   └── kustomization.yml
│       ├── deployment.yml
│       └── service.yml
├── overlays
│   └── production
│       ├── .gitkeep
│       ├── deployment.yml
│       ├── kustomization.yml
│       └── version.yml
├── specifications
│   └── production
│       ├── kustomization.yml
│       ├── replicas.yml
│       └── resources.yml
└── templates
    ├── base
    │   ├── deployment.yml
    │   └── service.yml
    └── overlays
        ├── deployment.yml
        ├── kustomization.yml
        └── version.yml
```

| Directory           | Description                                                                                                                                                    |
|---------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------|
| base/               | This directory contains the base configuration for the application. It includes Kubernetes manifests like `deployment.yml` and `service.yml`, along with a `kustomization.yml` file that defines the resources to include in the base configuration. |
| overlays/           | This directory contains environment-specific configurations. In this case, it includes a directory named `production`, which contains Kubernetes manifests specific to the production environment. |
| specifications/    | This directory contain specifications for different environments. Similar to `overlays/production`, it has a directory named `production` containing `kustomization.yml`, `replicas.yml`, and `resources.yml` files. These files is to define specifications for the production environment, such as resource limits, replicas, etc. |
| templates/          | This directory appears to contain templates for Kubernetes manifests. It has a `base` directory containing `deployment.yml` and `service.yml` templates, and an `overlays` directory containing `deployment.yml`, `kustomization.yml`, and `version.yml` templates. |
